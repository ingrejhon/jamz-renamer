# Import library
import os
import shutil
from pathlib import Path
script_dir = os.path.dirname(__file__)

# Declarate Variables
#path
rel_path = "\\3\\"
path_initial = os.getcwd()+"\\1\\"
path_second = os.getcwd()+"\\2\\"
path_final = os.getcwd()+"\\3\\"
#archives
archives3 = []
archives1 = []
archives2 = []

abs_file_path = os.path.join(script_dir, path_final)
abs_path_initial = os.path.join(script_dir, path_initial)

print ("JAMZ RENAME")
print ("by Jhon Morales")
print ("ingrejhon@gmail.com")
print ("\n")
# Functions
def saveFileNames1():
    for root, dirs, files in os.walk(abs_path_initial):
        for filename1 in files:
            if filename1=="Thumbs.db":
                break
            if filename1=="Thumbs-1.db":
                break
            #f_name,f_ext= os.path.splitext(filename1)
            archives1.append(filename1)
        #print(archives1)

def saveFileNames2():
    for root, dirs, files in os.walk(abs_path_initial):
        for filename2 in files:
            if filename2=="Thumbs.db":
                break
            if filename2=="Thumbs-1.db":
                break
            #f_name,f_ext= os.path.splitext(filename1)
            archives2.append(filename2)
        #print(archives1)

def saveFileNames3():
    for root, dirs, files in os.walk(abs_file_path):
        for filename3 in files:
            if filename3=="Thumbs.db":
                break
            if filename3=="Thumbs-1.db":
                break
            if filename3=="Thumbs-2.db":
                break
            archives3.append(filename3)
       # print(archives3)

def renameFiles():
    i=0
    j=0
    for filename in os.listdir(path_initial):
        if filename=="Thumbs.db":
            break
        if filename=="Thumbs-1.db":
            break
        fullpath1 = os.path.join(path_initial, filename)
        os.rename(fullpath1, path_initial+"\\"+archives3[(i)])
        i+=1
    for filename2 in os.listdir(path_second):
        if filename2=="Thumbs.db":
            break
        if filename2=="Thumbs-2.db":
            break
        fullpath2 = os.path.join(path_second, filename2)
        print(fullpath2)
        os.rename(fullpath2, path_second+"\\"+archives3[(j)])
        j+=1
    for filefolder1 in os.listdir(path_initial):
        fullpath3 = os.path.join(path_initial, filefolder1)
        f_name, f_ext= os.path.splitext(filefolder1)
        new_name= '{}{}'.format(f_name+"-1",f_ext)
        os.rename(fullpath3, path_initial+"\\"+new_name)
    for filefolder2 in os.listdir(path_second):
        fullpath4 = os.path.join(path_second, filefolder2)
        f_name2, f_ext2= os.path.splitext(filefolder2)
        new_name2= '{}{}'.format(f_name2+"-2",f_ext2)
        os.rename(fullpath4, path_second+"\\"+new_name2)
    for filefolder3 in os.listdir(path_final):
        fullpath5 = os.path.join(path_final, filefolder3)
        f_name3, f_ext3= os.path.splitext(filefolder3)
        new_name3= '{}{}'.format(f_name3+"-3",f_ext3)
        os.rename(fullpath5, path_final+"\\"+new_name3)


# Run Functions
saveFileNames1()
saveFileNames3()
#print(archives3)
renameFiles()

#Mensajes de finalizacion de procesos
#print ("ARCHIVOS RENOMBRADOS, PRESIONE ENTER PARA UNIFICAR LAS CARPETAS")
print("PROCESO TERMINADO PRESIONE UNA TECLA PARA SALIR")
os.system("pause")

